'use strict';

const usersData = require('../masterdata/users.json');

module.exports = {
  async up (queryInterface, Sequelize) {
    const users = usersData.map((eachUser) => {
        eachUser.createdAt = new Date();
        eachUser.updatedAt = new Date();
        return eachUser;
    })


    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */

    await queryInterface.bulkInsert('Users', users);
  },

  async down (queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    await queryInterface.bulkDelete('Users', null);
  }
};
