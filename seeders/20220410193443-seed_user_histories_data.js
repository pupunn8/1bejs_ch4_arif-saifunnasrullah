'use strict';

const data = require("../masterdata/user_histories.json");

module.exports = {
  async up (queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
     const histories = data.map((eachHistories) => {
        eachHistories.createdAt = new Date();
        eachHistories.updatedAt = new Date();
        return eachHistories;
    })

    await queryInterface.bulkInsert('User_histories', histories);
  },

  async down (queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
     await queryInterface.bulkDelete('User_histories', null);
  }
};
