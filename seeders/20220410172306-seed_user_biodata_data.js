'use strict';

const data = require("../masterdata/user_biodata.json")

module.exports = {
  async up (queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
     const biodata = data.map((eachBiodata) => {
        eachBiodata.createdAt = new Date();
        eachBiodata.updatedAt = new Date();
        return eachBiodata;
    })

    await queryInterface.bulkInsert('User_biodata', biodata);
  },

  async down (queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
     await queryInterface.bulkDelete('User_biodata', null);
  }
};
