const express = require('express');
const router = express.Router();
const { getAllUserBiodata, getBiodataById, createBiodata, updateBiodataById, deleteBiodataById } = require('../controller/user_biodata.controller');

router.get('/', getAllUserBiodata);
router.get('/:id', getBiodataById);
router.post('/', createBiodata);
router.post('/:id', updateBiodataById);
router.delete('/:id', deleteBiodataById);

module.exports = router;