const express = require('express');
const router = express.Router();
const {authentication} = require("../misc/middleware");
const userRoutes = require('./user.route');
const userBiodataRoutes = require('./user_biodata.route');
const userHistoriesRoutes = require('./user_histories.route');

router.get('/', (req, res) => {
    res.status(200).json({
        status: "Success",
        message: "Hello World!"
    });
});

router.use(authentication);

router.use('/users', userRoutes);
router.use('/userBiodata', userBiodataRoutes);
router.use('/histories', userHistoriesRoutes);

module.exports = router;