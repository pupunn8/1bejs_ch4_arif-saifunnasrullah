const express = require('express');
const router = express.Router();
const { getAllUserHistories, getHistoryById, createHistory, updateHistoryById, deleteHistoryById } = require('../controller/user_histories.controller');

router.get('/', getAllUserHistories);
router.get('/:id', getHistoryById);
router.post('/', createHistory);
router.post('/:id', updateHistoryById);
router.delete('/:id', deleteHistoryById);

module.exports = router;